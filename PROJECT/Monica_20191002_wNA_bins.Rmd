---
title: "EDA Monica Dataset"
author: "Marta Matosas Fonolleda"
date: "Oct 1, 2019"
output: html_document
---


```{r basicfcn, include=F}
# can add quietly=T option to the require() function
loadPkg = function(x) { if (!require(x,character.only=T, quietly =T)) { install.packages(x,dep=T,repos="http://cran.us.r-project.org"); if(!require(x,character.only=T)) stop("Package not found") } }
```

```{r setup, include=F}
knitr::opts_chunk$set(echo = TRUE)
monica <- data.frame(read.csv("C:/Users/Marta/Documents/monica.csv", header = TRUE, stringsAsFactors = F))
library(dplyr)
library(ggplot2)
library(plyr)
```

## Inspect dataset
```{r 1, include=T}
head(monica, 10)
tail(monica, 10)
str(monica)
```

Dataframe with 13 variables (including X, counter of number of samples) and 6367 samples.

Column 1 labeled "3" contains a counter of the samples.

12 variables to analyze: 3 are numeric - age and yronset - and the data type of the remaining 10 is characters.


**Inspect the 10 columns with data type characters**

```{r 2, include=T}
print('outcome')
table(monica$outcome)
print('sex')
table(monica$sex)
print('premi')
table(monica$premi)
print('smstat')
table(monica$smstat)
print('diabetes')
table(monica$diabetes)
print('highbp')
table(monica$highbp)
print('hicol')
table(monica$hichol)
print('angina')
table(monica$angina)
print('stroke')
table(monica$stroke)
print('hosp')
table(monica$hosp)
```
Large number of Not Known ("nk") values in 7 columns: "premi","smstat","diabetes","highbp","hichol","angina" and "stroke"

3 Columns with complete info: "outcome", "sex" and "hosp"

**Inspect the 2 columns with data type numeric**
```{r 3, include=T}
sum(is.na(monica$age))
sum(is.na(monica$yronset))
sum(is.null(monica$age))
sum(is.null(monica$yronset))
summary(monica$age)
summary(monica$yronset)
#describe(monica$age)
```
Both column "age" and "yronset" are complete, no misisng values.

**Drop column 1**

Drop redundant column (column # 1).
Check operation.

```{r 4, include=T}
monica <- monica[, 2:13]
str(monica)
```


Further inspect variable sex

```{r 5, include=T}
ggplot(aes(x = monica$sex),data = monica) +
    geom_bar(fill = c("green","blue")) +
     scale_x_discrete(labels = c("Females", "Males")) +
    ggtitle("Distribution of the sample by sex")
```
The data sample is skewed towards men. Note: does this reflect the frequency of the population? Define population.

Further inspect variable age

```{r 6, include=T}
ggplot(aes(x = age),data = monica) +
    geom_histogram(binwidth = 1,col = "black",fill = "red") +
    xlim(c(35,69)) +
    ggtitle("Distribution of the sample by age") +
    scale_x_sqrt()
```
Majority of the samples in this dataset correspond to people from 55 to 75 years old.

**Distribution of samples from 55 to 75 years by sex**
```{r 7, include=T}
monica55to75 <- subset(monica, age >= 55)
ggplot(aes(x = monica55to75$sex),data = monica55to75) +
    geom_bar(fill = c("green","blue")) +
     scale_x_discrete(labels = c("Females", "Males")) +
    ggtitle("Distribution of the sample by sex, patients 55 years old onwards")
```

The data of samples from 55 to 75 yeard old is skewed towards men as well.

**Distribution of samples by outcome**
```{r 8, include=T}
ggplot(aes(x = outcome),data = monica) +
    geom_bar(fill = c("black","orange")) +
     scale_x_discrete(labels = c("Dead", "Live")) +
    ggtitle("Distribution of the sample by outcome")
```
**Distribution of samples by hospotalization**
```{r 9, include=T}
ggplot(aes(x = hosp),data = monica) +
    geom_bar(fill = c("white","purple")) +
     scale_x_discrete(labels = c("Hospotalized", "Not hospitalized")) +
    ggtitle("Distribution of the sample by hospitalization")
```

**Recap:**
Variables with complete information: sex, age, outcome, hospitalization, and year onset.


## UNIVARATE ANALYSIS, CONVERTING NK VALUES TO NA

## convert nk values to NA
```{r 10, include=T}
#Copy dataset
monica.na <- monica
#Convert all nk in the dataset to NA
monica.na <- na_if(monica.na, 'nk')
#check conversion
str(monica.na)
table(monica.na$premi)
table(monica.na$smstat)
table(monica.na$diabetes)
table(monica.na$highbp)
table(monica.na$hichol)
table(monica.na$angina)
table(monica.na$stroke)
```

## Inspect each categorical variable by Outcome

This variable was not modified, was already complete.
p-value should be 0.9997 amd it is.

**Sex and Outcome**
```{r 11, include=T}
#Create table
table11 <- table(monica.na$sex, monica.na$outcome)
# Next perform chi-square test
chi.res11 <- chisq.test(table11)
chi.res11
```
The null hypothesis H0 is sex and outcome are independent.

p-value is greater than 0.05. 

Therefore, we fail to reject the null hypothesis.

**Previous myocardial infarction event (premi) and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 0.001374. We get the same.

```{r 12, include=T}
#Create table
table12 <- table(monica.na$premi, monica.na$outcome)
# Next perform chi-square test
chi.res12 <- chisq.test(table12)
chi.res12
```
The null hypothesis H0 is premi and outcome are independent.

p-value is smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that premi and outcome are not significantly independent.

**Smoking status (smstat) and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 0.03172. We get the same.

```{r 13, include=T}
#Create table
table13 <- table(monica.na$smstat, monica.na$outcome)
# Next perform chi-square test
chi.res13 <- chisq.test(table13)
chi.res13
```
The null hypothesis H0 is smstat and outcome are independent.

p-value is smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that premi and outcome are not significantly independent.

**Diabetes (diabetes) and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 5.472e-05. We get the same.

```{r 14, include=T}
#Create table
table14 <- table(monica.na$diabetes, monica.na$outcome)
# Next perform chi-square test
chi.res14 <- chisq.test(table14)
chi.res14
```
The null hypothesis H0 is diabetes and outcome are independent.

p-value is significantly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that diabetes and outcome are not significantly independent.

**High Blood Pressure (highbp) and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 0.1915. We get the same.


```{r 15, include=T}
#Create table
table15 <- table(monica.na$highbp, monica.na$outcome)
# Next perform chi-square test
chi.res15 <- chisq.test(table15)
chi.res15
```
The null hypothesis H0 is high blood pressure (highbp) and outcome are independent.

p-value is gretaer than 0.05.

Therefore, we fail to reject the null hypothesis H0.

**High Cholesterol (hichol) and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 1.182e-10. We get the same.

```{r 16, include=T}
#Create table
table16 <- table(monica.na$hichol, monica.na$outcome)
# Next perform chi-square test
chi.res16 <- chisq.test(table16)
chi.res16
```
The null hypothesis H0 is high cholesterol and outcome are independent.

p-value is significantly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that high cholesterol and outcome are not significantly independent.

**Angina and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 2.137e-07. We get the same.

```{r 17, include=T}
#Create table
table17 <- table(monica.na$angina, monica.na$outcome)
# Next perform chi-square test
chi.res17 <- chisq.test(table17)
chi.res17
```
The null hypothesis H0 is angina and outcome are independent.

p-value is significantly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that angina and outcome are not significantly independent.

**Stroke and Outcome**
This variable was modified, nk replaced with NA.
p-value of subset is 3.897e-13. We get the same.

```{r 18, include=T}
#Create table
table18 <- table(monica.na$stroke, monica.na$outcome)
# Next perform chi-square test
chi.res18 <- chisq.test(table18)
chi.res18
```
The null hypothesis H0 is stroke and outcome are independent.

p-value is significantly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that stroke and outcome are not significantly independent.


**Hospitalization (hosp) and Outcome**
This variable was not modified, was already complete.
p-value should be < 2.2e-16 amd it is.

```{r 19, include=T}
#Create table, no need to subset
table19 <- table(monica.na$hosp, monica.na$outcome)
# Next perform chi-square test
chi.res19 <- chisq.test(table19)
chi.res19
```

The null hypothesis H0 is hospitalization and outcome are independent.

p-value is significanly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that hospitalization and outcome are not significantly independent.

##SUMMARY OF UNIVARIATE ANALYSIS, target is Outcome

Variables for which we reject the null hypothesis Ho and conclude that they are not significantly independent:
premi, smstat, diabetes, high cholesterol, angina, stroke, hosp

of these, p-value is significantly smaller for: diabetes, high cholesterol, angina, stroke and hospitalization.

Variables for which we fail to reject the null hypothesis:
sex, high blood pressure

##SAME ANALYSIS BY SEX
```{r 20, include=T}
#Subset by sex
monica.f <- subset(monica.na, sex == 'f')
monica.m <- subset(monica.na, sex == 'm')
str(monica.f)
str(monica.m)
```
**Sex and Outcome by Sex**
Female
```{r 22.1, include=T}
#Create table
table22.1 <- table(monica.f$sex, monica.f$outcome)
# Next perform chi-square test
chi.res22.1 <- chisq.test(table22.1)
chi.res22.1
```
Male
```{r 22.2, include=T}
#Create table
table22.2 <- table(monica.m$sex, monica.m$outcome)
# Next perform chi-square test
chi.res22.2 <- chisq.test(table22.2)
chi.res22.2
```
Same conclusion for female and male

**Premi and Outcome by Sex**
Female
```{r 23.1, include=T}
#Create table
table23.1 <- table(monica.f$premi, monica.f$outcome)
# Next perform chi-square test
chi.res23.1 <- chisq.test(table23.1)
chi.res23.1
```
Male
```{r 23.2, include=T}
#Create table
table23.2 <- table(monica.m$premi, monica.m$outcome)
# Next perform chi-square test
chi.res23.2 <- chisq.test(table23.2)
chi.res23.2
```
Different conclusion for female and male!

**smstat and Outcome by Sex**
Female
```{r 24.1, include=T}
#Create table
table24.1 <- table(monica.f$smstat, monica.f$outcome)
# Next perform chi-square test
chi.res24.1 <- chisq.test(table24.1)
chi.res24.1
```
Male
```{r 24.2, include=T}
#Create table
table24.2 <- table(monica.m$smstat, monica.m$outcome)
# Next perform chi-square test
chi.res24.2 <- chisq.test(table24.2)
chi.res24.2
```
Different conclusion for female and male!

**Diabetes and Outcome by Sex**
Female
```{r 25.1, include=T}
#Create table
table25.1 <- table(monica.f$diabetes, monica.f$outcome)
# Next perform chi-square test
chi.res25.1 <- chisq.test(table25.1)
chi.res25.1
```
Male
```{r 25.2, include=T}
#Create table
table25.2 <- table(monica.m$diabetes, monica.m$outcome)
# Next perform chi-square test
chi.res25.2 <- chisq.test(table25.2)
chi.res25.2
```
Same conclusion for female and male.


**High Blood Pressure and Outcome by Sex**
Female
```{r 26.1, include=T}
#Create table
table26.1 <- table(monica.f$highbp, monica.f$outcome)
# Next perform chi-square test
chi.res26.1 <- chisq.test(table26.1)
chi.res26.1
```
Male
```{r 26.2, include=T}
#Create table
table26.2 <- table(monica.m$highbp, monica.m$outcome)
# Next perform chi-square test
chi.res26.2 <- chisq.test(table26.2)
chi.res26.2
```
Same conclusion for female and male.

**High Cholesterol and Outcome by Sex**
Female
```{r 27.1, include=T}
#Create table
table27.1 <- table(monica.f$hichol, monica.f$outcome)
# Next perform chi-square test
chi.res27.1 <- chisq.test(table27.1)
chi.res27.1
```
Male
```{r 27.2, include=T}
#Create table
table27.2 <- table(monica.m$hichol, monica.m$outcome)
# Next perform chi-square test
chi.res27.2 <- chisq.test(table27.2)
chi.res27.2
```
Same conclusion for female and male.


**Angina and Outcome by Sex**
Female
```{r 28.1, include=T}
#Create table
table28.1 <- table(monica.f$angina, monica.f$outcome)
# Next perform chi-square test
chi.res28.1 <- chisq.test(table28.1)
chi.res28.1
```
Male
```{r 28.2, include=T}
#Create table
table28.2 <- table(monica.m$angina, monica.m$outcome)
# Next perform chi-square test
chi.res28.2 <- chisq.test(table28.2)
chi.res28.2
```
Different conclusion for female and male!

**Stroke and Outcome by Sex**
Female
```{r 29.1, include=T}
#Create table
table29.1 <- table(monica.f$stroke, monica.f$outcome)
# Next perform chi-square test
chi.res29.1 <- chisq.test(table29.1)
chi.res29.1
```
Male
```{r 29.2, include=T}
#Create table
table29.2 <- table(monica.m$stroke, monica.m$outcome)
# Next perform chi-square test
chi.res29.2 <- chisq.test(table29.2)
chi.res29.2
```
same conclusion for female and male

**Hosp and Outcome by Sex**
Female
```{r 30.1, include=T}
#Create table
table30.1 <- table(monica.f$hosp, monica.f$outcome)
# Next perform chi-square test
chi.res30.1 <- chisq.test(table30.1)
chi.res30.1
```
Male
```{r 30.2, include=T}
#Create table
table30.2 <- table(monica.m$hosp, monica.m$outcome)
# Next perform chi-square test
chi.res30.2 <- chisq.test(table30.2)
chi.res30.2
```
same conclusion for female and male

##Study of missingness by sex & feature

```{r 31.1, include=T}
#Subset dataset by sex
monica.fnk <- subset(monica, monica$sex == 'f')
monica.mnk <- subset(monica, monica$sex == 'm')
#premi
print('premi / female')
table(monica.fnk$premi)
print('premi / male')
table(monica.mnk$premi)
#smstat
print('smstat / female')
table(monica.fnk$smstat)
print('smstat / male')
table(monica.mnk$smstat)
#diabetes
print('diabetes / female')
table(monica.fnk$diabetes)
print('diabetes / male')
table(monica.mnk$diabetes)
#highbp
print('highbp / female')
table(monica.fnk$highbp)
print('highbp / male')
table(monica.mnk$highbp)
#hichol
print('hichol / female')
table(monica.fnk$hichol)
print('hichol / male')
table(monica.mnk$hichol)
#angina
print('angina / female')
table(monica.fnk$angina)
print('angina / male')
table(monica.mnk$angina)
#stroke
print('stroke / female')
table(monica.fnk$stroke)
print('stroke / male')
table(monica.mnk$stroke)
```

##Analysis numerical variabes Age and Yronset

**Prepare dataframe**

```{r 32.1, include=T}
#copy dataframe
monica.na.bin <- monica.na
#inspect age
summary(monica.na.bin$age)
#bin age in new column
monica.na.bin$agecat <- cut(monica.na.bin$age, c(34.5, 39.5, 44.5, 49.5, 54.5, 59.5, 64.5, 69.5))
summary(monica.na.bin$agecat)
str(monica.na.bin)
```

```{r 32.2, include=T}
#inspect yronset
summary(monica.na.bin$yronset)
#bin yronset in new column
monica.na.bin$yronsetcat <- cut(monica.na.bin$yronset, c(8.45, 89.5, 94.5))
summary(monica.na.bin$yronsetcat)
str(monica.na.bin)
```
```{r 32.3, include=T}

#Subset by sex
monica.na.bin.f <- subset(monica.na.bin, sex == 'f')
monica.na.bin.m <- subset(monica.na.bin, sex == 'm')
str(monica.na.bin.f)
str(monica.na.bin.m)
```

**Analysis of age / outcome, and age / outcome / sex**
**Age / outcome**
```{r 33, include=T}
#Create table
table33 <- table(monica.na.bin$agecat, monica.na.bin$outcome)
# Next perform chi-square test
chi.res33 <- chisq.test(table33)
chi.res33
```
The null hypothesis H0 is age and outcome are independent.

p-value is significanly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that age and outcome are not significantly independent.

**Age / outcome / sex**
**Female**
```{r 34, include=T}
#Create table
table34 <- table(monica.na.bin.f$agecat, monica.na.bin.f$outcome)
# Next perform chi-square test
chi.res34 <- chisq.test(table34)
chi.res34
```
The null hypothesis H0 is age and outcome are independent.

p-value is significanly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that age and outcome are not significantly independent.

**Male**
```{r 35, include=T}
#Create table
table35 <- table(monica.na.bin.m$agecat, monica.na.bin.m$outcome)
# Next perform chi-square test
chi.res35 <- chisq.test(table35)
chi.res35
```
The null hypothesis H0 is age and outcome are independent.

p-value is smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that age and outcome are not significantly independent.


##Analysis yronset / outcome and yronset / outcome / sex

```{r 36, include=T}
#Create table
table36 <- table(monica.na.bin$yronsetcat, monica.na.bin$outcome)
# Next perform chi-square test
chi.res36 <- chisq.test(table36)
chi.res36
```

The null hypothesis H0 is yronset and outcome are independent.

p-value is significanly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that yronset and outcome are not significantly independent.

**Fenale**
```{r 37, include=T}
#Create table
table37 <- table(monica.na.bin.f$yronsetcat, monica.na.bin.f$outcome)
# Next perform chi-square test
chi.res37 <- chisq.test(table37)
chi.res37
```
The null hypothesis H0 is yronset and outcome are independent.

p-value is slightly smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that yronset and outcome are not significantly independent.


**Male**
```{r 38, include=T}
#Create table
table38 <- table(monica.na.bin.m$yronsetcat, monica.na.bin.m$outcome)
# Next perform chi-square test
chi.res38 <- chisq.test(table38)
chi.res38
```
The null hypothesis H0 is yronset and outcome are independent.

p-value is smaller than 0.05.

Therefore, we reject the null hypothesis H0 and conclude that yronset and outcome are not significantly independent.

##Univariate losgistic regression analysis
**Results converting target to numerical**
```{r 39, include=T}
#copy dataset
monica.co <- monica.na
#create new column and convert outcome to 0 and 1
monica.co$coutcome <- monica$outcome
monica.co$outcome <-  revalue(monica.co$outcome, c("live"=1, "dead"=0))
monica.co$outcome <- as.factor(monica.co$outcome)
#inspect outcome
summary(monica.co$outcome)
# Next perform logistic regression
glm(outcome ~ diabetes, data = monica.co, family = "binomial")
```
**Results NOT converting target to numerical**
```{r 40, include=T}
# Perform logistic regression
glm(outcome ~ diabetes, data = monica.na, family = "binomial")
```
We get an error
Conclusion: target variable MUST be 0 - 1
What if it is 0 - 1 but categorical?

```{r 41, include=T}
#copy dataframe to a new dataframe
monica.co2 <- monica.na
#create new column and convert outcome to 0 and 1
monica.co2$coutcome <- monica$outcome
monica.co2$outcome <-  revalue(monica.co2$outcome, c("live"=1, "dead"=0))
#monica.co$outcome <- as.factor(monica.co$outcome)
#inspect outcome
summary(monica.co2$outcome)
# Next perform logistic regression
glm(outcome ~ diabetes, data = monica.co2, family = "binomial")
```

We get an error as well

COnclucion: target (y) MUST be converted to 0 - 1 and numerical
